<?php

namespace Drupal\outline_entity;

use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the outline entity type.
 */
class EntryViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['outline_entry_field_data']['table']['base']['help'] = $this->t('Outline entries are attached to nodes.');
    $data['outline_entry_field_data']['table']['base']['access query tag'] = 'entry_access';
    $data['outline_entry_field_data']['table']['wizard_id'] = 'outline_entry';

    $data['outline_entry_field_data']['table']['join'] = array(
      // This is provided for the many_to_one argument.
      'outline_index' => array(
        'field' => 'eid',
        'left_field' => 'eid',
      ),
    );

    $data['outline_entry_field_data']['eid']['help'] = $this->t('The eid of an outline entry.');

    $data['outline_entry_field_data']['eid']['argument']['id'] = 'outline';
    $data['outline_entry_field_data']['eid']['argument']['name field'] = 'name';
    $data['outline_entry_field_data']['eid']['argument']['zero is null'] = TRUE;

    $data['outline_entry_field_data']['eid']['filter']['id'] = 'outline_index_eid';
    $data['outline_entry_field_data']['eid']['filter']['title'] = $this->t('Entry');
    $data['outline_entry_field_data']['eid']['filter']['help'] = $this->t('Outline entry chosen from autocomplete or select widget.');
    $data['outline_entry_field_data']['eid']['filter']['hierarchy table'] = 'outline_entry_hierarchy';
    $data['outline_entry_field_data']['eid']['filter']['numeric'] = TRUE;

    $data['outline_entry_field_data']['eid_raw'] = array(
      'title' => $this->t('Entry ID'),
      'help' => $this->t('The eid of an outline entry.'),
      'real field' => 'eid',
      'filter' => array(
        'id' => 'numeric',
        'allow empty' => TRUE,
      ),
    );

    $data['outline_entry_field_data']['eid_representative'] = array(
      'relationship' => array(
        'title' => $this->t('Representative node'),
        'label'  => $this->t('Representative node'),
        'help' => $this->t('Obtains a single representative node for each entry, according to a chosen sort criterion.'),
        'id' => 'groupwise_max',
        'relationship field' => 'eid',
        'outer field' => 'outline_entry_field_data.eid',
        'argument table' => 'outline_entry_field_data',
        'argument field' => 'eid',
        'base'   => 'node_field_data',
        'field'  => 'nid',
        'relationship' => 'node_field_data:entry_node_eid'
      ),
    );

    $data['outline_entry_field_data']['oid']['help'] = $this->t('Filter the results of "Outline: Entry" to a particular outline.');
    unset($data['outline_entry_field_data']['oid']['field']);
    unset($data['outline_entry_field_data']['oid']['argument']);
    unset($data['outline_entry_field_data']['oid']['sort']);

    $data['outline_entry_field_data']['name']['field']['id'] = 'entry_name';
    $data['outline_entry_field_data']['name']['argument']['many to one'] = TRUE;
    $data['outline_entry_field_data']['name']['argument']['empty field name'] = $this->t('Uncategorized');

    $data['outline_entry_field_data']['description__value']['field']['click sortable'] = FALSE;

    $data['outline_entry_field_data']['changed']['title'] = $this->t('Updated date');
    $data['outline_entry_field_data']['changed']['help'] = $this->t('The date the entry was last updated.');

    $data['outline_entry_field_data']['changed_fulldate'] = array(
      'title' => $this->t('Updated date'),
      'help' => $this->t('Date in the form of CCYYMMDD.'),
      'argument' => array(
        'field' => 'changed',
        'id' => 'date_fulldate',
      ),
    );

    $data['outline_entry_field_data']['changed_year_month'] = array(
      'title' => $this->t('Updated year + month'),
      'help' => $this->t('Date in the form of YYYYMM.'),
      'argument' => array(
        'field' => 'changed',
        'id' => 'date_year_month',
      ),
    );

    $data['outline_entry_field_data']['changed_year'] = array(
      'title' => $this->t('Updated year'),
      'help' => $this->t('Date in the form of YYYY.'),
      'argument' => array(
        'field' => 'changed',
        'id' => 'date_year',
      ),
    );

    $data['outline_entry_field_data']['changed_month'] = array(
      'title' => $this->t('Updated month'),
      'help' => $this->t('Date in the form of MM (01 - 12).'),
      'argument' => array(
        'field' => 'changed',
        'id' => 'date_month',
      ),
    );

    $data['outline_entry_field_data']['changed_day'] = array(
      'title' => $this->t('Updated day'),
      'help' => $this->t('Date in the form of DD (01 - 31).'),
      'argument' => array(
        'field' => 'changed',
        'id' => 'date_day',
      ),
    );

    $data['outline_entry_field_data']['changed_week'] = array(
      'title' => $this->t('Updated week'),
      'help' => $this->t('Date in the form of WW (01 - 53).'),
      'argument' => array(
        'field' => 'changed',
        'id' => 'date_week',
      ),
    );

    $data['outline_index']['table']['group']  = $this->t('Outline entry');

    $data['outline_index']['table']['join'] = array(
      'outline_entry_field_data' => array(
        // links directly to outline_entry_field_data via eid
        'left_field' => 'eid',
        'field' => 'eid',
      ),
      'node_field_data' => array(
        // links directly to node via nid
        'left_field' => 'nid',
        'field' => 'nid',
      ),
      'outline_entry_hierarchy' => array(
        'left_field' => 'eid',
        'field' => 'eid',
      ),
    );

    $data['outline_index']['nid'] = array(
      'title' => $this->t('Content with entry'),
      'help' => $this->t('Relate all content tagged with an entry.'),
      'relationship' => array(
        'id' => 'standard',
        'base' => 'node',
        'base field' => 'nid',
        'label' => $this->t('node'),
        'skip base' => 'node',
      ),
    );

    // @todo This stuff needs to move to a node field since really it's all
    //   about nodes.
    $data['outline_index']['eid'] = array(
      'group' => $this->t('Content'),
      'title' => $this->t('Has outline entry ID'),
      'help' => $this->t('Display content if it has the selected outline entries.'),
      'argument' => array(
        'id' => 'outline_index_eid',
        'name table' => 'outline_entry_field_data',
        'name field' => 'name',
        'empty field name' => $this->t('Uncategorized'),
        'numeric' => TRUE,
        'skip base' => 'outline_entry_field_data',
      ),
      'filter' => array(
        'title' => $this->t('Has outline entry'),
        'id' => 'outline_index_eid',
        'hierarchy table' => 'outline_entry_hierarchy',
        'numeric' => TRUE,
        'skip base' => 'outline_entry_field_data',
        'allow empty' => TRUE,
      ),
    );

    $data['outline_index']['status'] = [
      'title' => $this->t('Publish status'),
      'help' => $this->t('Whether or not the content related to an entry is published.'),
      'filter' => [
        'id' => 'boolean',
        'label' => $this->t('Published status'),
        'type' => 'yes-no',
      ],
    ];

    $data['outline_index']['sticky'] = [
      'title' => $this->t('Sticky status'),
      'help' => $this->t('Whether or not the content related to an entry is sticky.'),
      'filter' => [
        'id' => 'boolean',
        'label' => $this->t('Sticky status'),
        'type' => 'yes-no',
      ],
      'sort' => [
        'id' => 'standard',
        'help' => $this->t('Whether or not the content related to an entry is sticky. To list sticky content first, set this to descending.'),
      ],
    ];

    $data['outline_index']['created'] = [
      'title' => $this->t('Post date'),
      'help' => $this->t('The date the content related to an entry was posted.'),
      'sort' => [
        'id' => 'date'
      ],
      'filter' => [
        'id' => 'date',
      ],
    ];

    $data['outline_entry_hierarchy']['table']['group']  = $this->t('Outline entry');
    $data['outline_entry_hierarchy']['table']['provider']  = 'outline';

    $data['outline_entry_hierarchy']['table']['join'] = array(
      'outline_entry_hierarchy' => array(
        // Link to self through left.parent = right.eid (going down in depth).
        'left_field' => 'eid',
        'field' => 'parent',
      ),
      'outline_entry_field_data' => array(
        // Link directly to outline_entry_field_data via eid.
        'left_field' => 'eid',
        'field' => 'eid',
      ),
    );

    $data['outline_entry_hierarchy']['parent'] = array(
      'title' => $this->t('Parent entry'),
      'help' => $this->t('The parent entry of the entry. This can produce duplicate entries if you are using an outline that allows multiple parents.'),
      'relationship' => array(
        'base' => 'outline_entry_field_data',
        'field' => 'parent',
        'label' => $this->t('Parent'),
        'id' => 'standard',
      ),
      'filter' => array(
        'help' => $this->t('Filter the results of "Outline: Entry" by the parent pid.'),
        'id' => 'numeric',
      ),
      'argument' => array(
        'help' => $this->t('The parent entry of the entry.'),
        'id' => 'outline',
      ),
    );

    return $data;
  }

}
