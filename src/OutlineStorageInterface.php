<?php

namespace Drupal\outline_entity;

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;

/**
 * Defines an interface for outline entity storage classes.
 */
interface OutlineStorageInterface extends ConfigEntityStorageInterface {

  /**
   * Gets top-level entry IDs of outlines.
   *
   * @param array $oids
   *   Array of outline IDs.
   *
   * @return array
   *   Array of top-level entry IDs.
   */
  public function getToplevelEids($oids);

}
