<?php

namespace Drupal\outline_entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines an interface for outline_entry entity storage classes.
 */
interface EntryStorageInterface extends ContentEntityStorageInterface {

  /**
   * Removed reference to entries from entry_hierarchy.
   *
   * @param array $eids
   *   Array of entries that need to be removed from hierarchy.
   */
  public function deleteEntryHierarchy($eids);

  /**
   * Updates entries hierarchy information with the hierarchy trail of it.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entry
   *   Entry entity that needs to be added to entry hierarchy information.
   */
  public function updateEntryHierarchy(EntityInterface $entry);

  /**
   * Finds all parents of a given entry ID.
   *
   * @param int $eid
   *   Entry ID to retrieve parents for.
   *
   * @return \Drupal\outline_entity\EntryInterface[]
   *   An array of entry objects which are the parents of the entry $eid.
   */
  public function loadParents($eid);

  /**
   * Finds all ancestors of a given entry ID.
   *
   * @param int $eid
   *   Entry ID to retrieve ancestors for.
   *
   * @return \Drupal\outline_entity\EntryInterface[]
   *   An array of entry objects which are the ancestors of the entry $eid.
   */
  public function loadAllParents($eid);

  /**
   * Finds all children of an entry ID.
   *
   * @param int $eid
   *   Entry ID to retrieve parents for.
   * @param string $oid
   *   An optional outline ID to restrict the child search.
   *
   * @return \Drupal\outline_entity\EntryInterface[]
   *   An array of entry objects that are the children of the entry $eid.
   */
  public function loadChildren($eid, $oid = NULL);

  /**
   * Finds all entries in a given outline ID.
   *
   * @param string $oid
   *   Outline ID to retrieve entries for.
   * @param int $parent
   *   The entry ID under which to generate the tree. If 0, generate the tree
   *   for the entire outline.
   * @param int $max_depth
   *   The number of levels of the tree to return. Leave NULL to return all
   *   levels.
   * @param bool $load_entities
   *   If TRUE, a full entity load will occur on the entry objects. Otherwise
   *   they are partial objects queried directly from the {outline_entry_data}
   *   table to save execution time and memory consumption when listing large
   *   numbers of entries. Defaults to FALSE.
   *
   * @return object[]|\Drupal\outline_entity\EntryInterface[]
   *   An array of entry objects that are the children of the outline $oid.
   */
  public function loadTree($oid, $parent = 0, $max_depth = NULL, $load_entities = FALSE);

  /**
   * Count the number of nodes in a given outline ID.
   *
   * @param string $oid
   *   Outline ID to retrieve entries for.
   *
   * @return int
   *   A count of the nodes in a given outline ID.
   */
  public function nodeCount($oid);

  /**
   * Reset the weights for a given outline ID.
   *
   * @param string $oid
   *   Outline ID to retrieve entries for.
   */
  public function resetWeights($oid);

  /**
   * Returns all entries used to tag some given nodes.
   *
   * @param array $nids
   *   Node IDs to retrieve entries for.
   * @param array $outlines
   *   (optional) An outlines array to restrict the entry search. Defaults to
   *   empty array.
   * @param string $langcode
   *   (optional) A language code to restrict the entry search. Defaults to NULL.
   *
   * @return array
   *   An array of nids and the entry entities they were tagged with.
   */
  public function getNodeEntrys(array $nids, array $outlines = array(), $langcode = NULL);

}
