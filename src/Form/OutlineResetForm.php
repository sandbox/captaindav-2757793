<?php

namespace Drupal\outline_entity\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\outline_entity\EntryStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides confirmation form for resetting an outline to alphabetical order.
 */
class OutlineResetForm extends EntityConfirmFormBase {

  /**
   * The entry storage.
   *
   * @var \Drupal\outline_entity\EntryStorageInterface
   */
  protected $entryStorage;

  /**
   * Constructs a new OutlineResetForm object.
   *
   * @param \Drupal\outline_entity\EntryStorageInterface $entry_storage
   *   The entry storage.
   */
  public function __construct(EntryStorageInterface $entry_storage) {
    $this->entryStorage = $entry_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager')->getStorage('outline_entry')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'outline_confirm_reset_alphabetical';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to reset the outline %title to alphabetical order?', array('%title' => $this->entity->label()));
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->entity->urlInfo('overview-form');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Resetting an outline will discard all custom ordering and sort items alphabetically.');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Reset to alphabetical');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->entryStorage->resetWeights($this->entity->id());

    drupal_set_message($this->t('Reset outline %name to alphabetical order.', array('%name' => $this->entity->label())));
    $this->logger('outline')->notice('Reset outline %name to alphabetical order.', array('%name' => $this->entity->label()));
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
