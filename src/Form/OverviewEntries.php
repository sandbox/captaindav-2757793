<?php

namespace Drupal\outline_entity\Form;

use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\outline_entity\OutlineInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/*
 * Provides entries overview form for an outline.
 */
class OverviewEntries extends FormBase {

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The entry storage handler.
   *
   * @var \Drupal\outline_entity\EntryStorageInterface
   */
  protected $storageController;

  /**
   * Constructs an OverviewEntries object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager service.
   */
  public function __construct(ModuleHandlerInterface $module_handler, EntityManagerInterface $entity_manager) {
    $this->moduleHandler = $module_handler;
    $this->storageController = $entity_manager->getStorage('outline_entry');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('entity.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'outline_overview_entries';
  }

  /**
   * Form constructor.
   *
   * Display a tree of all the entries in an outline, with options to edit
   * each one. The form is made drag and drop by the theme function.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\outline_entity\OutlineInterface $outline
   *   The outline to display the overview form for.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, OutlineInterface $outline = NULL) {
    // @todo Remove global variables when https://www.drupal.org/node/2044435 is
    //   in.
    global $pager_page_array, $pager_total, $pager_total_items;

    $form_state->set(['outline', 'outline'], $outline);
    $parent_fields = FALSE;

    $page = $this->getRequest()->query->get('page') ?: 0;
    // Number of entries per page.
    $page_increment = $this->config('outline_entry.settings')->get('entries_per_page_admin');
    $page_increment = 10;
    // Elements shown on this page.
    $page_entries = 0;
    // Elements at the root level before this page.
    $before_entries = 0;
    // Elements at the root level after this page.
    $after_entries = 0;
    // Elements at the root level on this page.
    $root_entries = 0;

    // Entries from previous and next pages are shown if the entry tree would have
    // been cut in the middle. Keep track of how many extra entries we show on
    // each page of entries.
    $back_step = NULL;
    $forward_step = 0;

    // An array of the entries to be displayed on this page.
    $current_page = array();

    $delta = 0;
    $entry_deltas = array();
    $tree = $this->storageController->loadTree($outline->id(), 0, NULL, TRUE);
    $tree_index = 0;
    do {
      // In case this tree is completely empty.
      if (empty($tree[$tree_index])) {
        break;
      }
      $delta++;
      // Count entries before the current page.
      if ($page && ($page * $page_increment) > $before_entries && !isset($back_step)) {
        $before_entries++;
        continue;
      }
      // Count entries after the current page.
      elseif ($page_entries > $page_increment && isset($complete_tree)) {
        $after_entries++;
        continue;
      }

      // Do not let an entry start the page that is not at the root.
      $entry = $tree[$tree_index];
      if (isset($entry->depth) && ($entry->depth > 0) && !isset($back_step)) {
        $back_step = 0;
        while ($pentry = $tree[--$tree_index]) {
          $before_entries--;
          $back_step++;
          if ($pentry->depth == 0) {
            $tree_index--;
            // Jump back to the start of the root level parent.
            continue 2;
          }
        }
      }
      $back_step = isset($back_step) ? $back_step : 0;

      // Continue rendering the tree until we reach the a new root item.
      if ($page_entries >= $page_increment + $back_step + 1 && $entry->depth == 0 && $root_entries > 1) {
        $complete_tree = TRUE;
        // This new item at the root level is the first item on the next page.
        $after_entries++;
        continue;
      }
      if ($page_entries >= $page_increment + $back_step) {
        $forward_step++;
      }

      // Finally, if we've gotten down this far, we're rendering an entry on this
      // page.
      $page_entries++;
      $entry_deltas[$entry->id()] = isset($entry_deltas[$entry->id()]) ? $entry_deltas[$entry->id()] + 1 : 0;
      $key = 'eid:' . $entry->id() . ':' . $entry_deltas[$entry->id()];

      // Keep track of the first entry displayed on this page.
      if ($page_entries == 1) {
        $form['#first_eid'] = $entry->id();
      }
      // Keep a variable to make sure at least 2 root elements are displayed.
      if ($entry->parents[0] == 0) {
        $root_entries++;
      }
      $current_page[$key] = $entry;
    } while (isset($tree[++$tree_index]));

    // Because we didn't use a pager query, set the necessary pager variables.
    $total_entries = $before_entries + $page_entries + $after_entries;
    $pager_total_items[0] = $total_entries;
    $pager_page_array[0] = $page;
    $pager_total[0] = ceil($total_entries / $page_increment);

    // If this form was already submitted once, it's probably hit a validation
    // error. Ensure the form is rebuilt in the same order as the user
    // submitted.
    $user_input = $form_state->getUserInput();
    if (!empty($user_input)) {
      // Get the POST order.
      $order = array_flip(array_keys($user_input['entries']));
      // Update our form with the new order.
      $current_page = array_merge($order, $current_page);
      foreach ($current_page as $key => $entry) {
        // Verify this is an entry for the current page and set at the current
        // depth.
        if (is_array($user_input['entries'][$key]) && is_numeric($user_input['entries'][$key]['entry']['eid'])) {
          $current_page[$key]->depth = $user_input['entries'][$key]['entry']['depth'];
        }
        else {
          unset($current_page[$key]);
        }
      }
    }

    $errors = $form_state->getErrors();
    $destination = $this->getDestinationArray();
    $row_position = 0;
    // Build the actual form.
    $form['entries'] = array(
      '#type' => 'table',
      '#header' => array($this->t('Name'), $this->t('Weight'), $this->t('Operations')),
      '#empty' => $this->t('No entries available. <a href=":link">Add entry</a>.', array(':link' => $this->url('entity.outline_entry.add_form', array('outline' => $outline->id())))),
      '#attributes' => array(
        'id' => 'outline',
      ),
    );
    foreach ($current_page as $key => $entry) {
      /** @var $entry \Drupal\Core\Entity\EntityInterface */
      $form['entries'][$key]['#entry'] = $entry;
      $indentation = array();
      if (isset($entry->depth) && $entry->depth > 0) {
        $indentation = array(
          '#theme' => 'indentation',
          '#size' => $entry->depth,
        );
      }
      $form['entries'][$key]['entry'] = array(
        '#prefix' => !empty($indentation) ? drupal_render($indentation) : '',
        '#type' => 'link',
        '#title' => $entry->getName(),
        '#url' => $entry->urlInfo(),
      );
      if ($outline->getHierarchy() != OUTLINE_HIERARCHY_MULTIPLE && count($tree) > 1) {
        $parent_fields = TRUE;
        $form['entries'][$key]['entry']['eid'] = array(
          '#type' => 'hidden',
          '#value' => $entry->id(),
          '#attributes' => array(
            'class' => array('entry-id'),
          ),
        );
        $form['entries'][$key]['entry']['parent'] = array(
          '#type' => 'hidden',
          // Yes, default_value on a hidden. It needs to be changeable by the
          // javascript.
          '#default_value' => $entry->parents[0],
          '#attributes' => array(
            'class' => array('entry-parent'),
          ),
        );
        $form['entries'][$key]['entry']['depth'] = array(
          '#type' => 'hidden',
          // Same as above, the depth is modified by javascript, so it's a
          // default_value.
          '#default_value' => $entry->depth,
          '#attributes' => array(
            'class' => array('entry-depth'),
          ),
        );
      }
      $form['entries'][$key]['weight'] = array(
        '#type' => 'weight',
        '#delta' => $delta,
        '#title' => $this->t('Weight for added entry'),
        '#title_display' => 'invisible',
        '#default_value' => $entry->getWeight(),
        '#attributes' => array(
          'class' => array('entry-weight'),
        ),
      );
//       dpm($destination);
//       dpm($entry->urlInfo('edit-form'));
      $operations = array(
        'edit2' => array(
          'title' => $this->t('Edit'),
          'query' => $destination,
          'url' => $entry->urlInfo('edit-form'),
        ),
        'delete' => array(
          'title' => $this->t('Delete'),
          'query' => $destination,
          'url' => $entry->urlInfo('delete-form'),
        ),
      );
      if ($this->moduleHandler->moduleExists('content_translation') && content_translation_translate_access($entry)->isAllowed()) {
        $operations['translate'] = array(
          'title' => $this->t('Translate'),
          'query' => $destination,
          'url' => $entry->urlInfo('drupal:content-translation-overview'),
        );
      }
      $form['entries'][$key]['operations'] = array(
        '#type' => 'operations',
        '#links' => $operations,
      );

      $form['entries'][$key]['#attributes']['class'] = array();
      if ($parent_fields) {
        $form['entries'][$key]['#attributes']['class'][] = 'draggable';
      }

      // Add classes that mark which entries belong to previous and next pages.
      if ($row_position < $back_step || $row_position >= $page_entries - $forward_step) {
        $form['entries'][$key]['#attributes']['class'][] = 'outline-entry-preview';
      }

      if ($row_position !== 0 && $row_position !== count($tree) - 1) {
        if ($row_position == $back_step - 1 || $row_position == $page_entries - $forward_step - 1) {
          $form['entries'][$key]['#attributes']['class'][] = 'outline-entry-divider-top';
        }
        elseif ($row_position == $back_step || $row_position == $page_entries - $forward_step) {
          $form['entries'][$key]['#attributes']['class'][] = 'outline-entry-divider-bottom';
        }
      }

      // Add an error class if this row contains a form error.
      foreach ($errors as $error_key => $error) {
        if (strpos($error_key, $key) === 0) {
          $form['entries'][$key]['#attributes']['class'][] = 'error';
        }
      }
      $row_position++;
    }

    if ($parent_fields) {
      $form['entries']['#tabledrag'][] = array(
        'action' => 'match',
        'relationship' => 'parent',
        'group' => 'entry-parent',
        'subgroup' => 'entry-parent',
        'source' => 'entry-id',
        'hidden' => FALSE,
      );
      $form['entries']['#tabledrag'][] = array(
        'action' => 'depth',
        'relationship' => 'group',
        'group' => 'entry-depth',
        'hidden' => FALSE,
      );
      $form['entries']['#attached']['library'][] = 'outline/drupal.outline';
      $form['entries']['#attached']['drupalSettings']['outline'] = [
        'backStep' => $back_step,
        'forwardStep' => $forward_step,
      ];
    }
    $form['entries']['#tabledrag'][] = array(
      'action' => 'order',
      'relationship' => 'sibling',
      'group' => 'entry-weight',
    );

    if ($outline->getHierarchy() != OUTLINE_HIERARCHY_MULTIPLE && count($tree) > 1) {
      $form['actions'] = array('#type' => 'actions', '#tree' => FALSE);
      $form['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => $this->t('Save'),
        '#button_type' => 'primary',
      );
      $form['actions']['reset_alphabetical'] = array(
        '#type' => 'submit',
        '#submit' => array('::submitReset'),
        '#value' => $this->t('Reset to alphabetical'),
      );
    }

    $form['pager_pager'] = ['#type' => 'pager'];
    return $form;
  }

  /**
   * Form submission handler.
   *
   * Rather than using a textfield or weight field, this form depends entirely
   * upon the order of form elements on the page to deentryine new weights.
   *
   * Because there might be hundreds or thousands of outline entries that need to
   * be ordered, entries are weighted from 0 to the number of entries in the
   * outline, rather than the standard -10 to 10 scale. Numbers are sorted
   * lowest to highest, but are not necessarily sequential. Numbers may be
   * skipped when an entry has children so that reordering is minimal when a child
   * is added or removed from an entry.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Sort entry order based on weight.
    uasort($form_state->getValue('entries'), array('Drupal\Component\Utility\SortArray', 'sortByWeightElement'));

    $outline = $form_state->get(['outline', 'outline']);
    // Update the current hierarchy type as we go.
    $hierarchy = OUTLINE_HIERARCHY_DISABLED;

    $changed_entries = array();
    $tree = $this->storageController->loadTree($outline->id(), 0, NULL, TRUE);

    if (empty($tree)) {
      return;
    }

    // Build a list of all entries that need to be updated on previous pages.
    $weight = 0;
    $entry = $tree[0];
    while ($entry->id() != $form['#first_eid']) {
      if ($entry->parents[0] == 0 && $entry->getWeight() != $weight) {
        $entry->setWeight($weight);
        $changed_entries[$entry->id()] = $entry;
      }
      $weight++;
      $hierarchy = $entry->parents[0] != 0 ? OUTLINE_HIERARCHY_SINGLE : $hierarchy;
      $entry = $tree[$weight];
    }

    // Renumber the current page weights and assign any new parents.
    $level_weights = array();
    foreach ($form_state->getValue('entries') as $eid => $values) {
      if (isset($form['entries'][$eid]['#entry'])) {
        $entry = $form['entries'][$eid]['#entry'];
        // Give entries at the root level a weight in sequence with entries on previous pages.
        if ($values['entry']['parent'] == 0 && $entry->getWeight() != $weight) {
          $entry->setWeight($weight);
          $changed_entries[$entry->id()] = $entry;
        }
        // Entries not at the root level can safely start from 0 because they're all on this page.
        elseif ($values['entry']['parent'] > 0) {
          $level_weights[$values['entry']['parent']] = isset($level_weights[$values['entry']['parent']]) ? $level_weights[$values['entry']['parent']] + 1 : 0;
          if ($level_weights[$values['entry']['parent']] != $entry->getWeight()) {
            $entry->setWeight($level_weights[$values['entry']['parent']]);
            $changed_entries[$entry->id()] = $entry;
          }
        }
        // Update any changed parents.
        if ($values['entry']['parent'] != $entry->parents[0]) {
          $entry->parent->target_id = $values['entry']['parent'];
          $changed_entries[$entry->id()] = $entry;
        }
        $hierarchy = $entry->parents[0] != 0 ? OUTLINE_HIERARCHY_SINGLE : $hierarchy;
        $weight++;
      }
    }

    // Build a list of all entries that need to be updated on following pages.
    for ($weight; $weight < count($tree); $weight++) {
      $entry = $tree[$weight];
      if ($entry->parents[0] == 0 && $entry->getWeight() != $weight) {
        $entry->parent->target_id = $entry->parents[0];
        $entry->setWeight($weight);
        $changed_entries[$entry->id()] = $entry;
      }
      $hierarchy = $entry->parents[0] != 0 ? OUTLINE_HIERARCHY_SINGLE : $hierarchy;
    }

    // Save all updated entries.
    foreach ($changed_entries as $entry) {
      $entry->save();
    }

    // Update the outline hierarchy to flat or single hierarchy.
    if ($outline->getHierarchy() != $hierarchy) {
      $outline->setHierarchy($hierarchy);
      $outline->save();
    }
    drupal_set_message($this->t('The configuration options have been saved.'));
  }

  /**
   * Redirects to confirmation form for the reset action.
   */
  public function submitReset(array &$form, FormStateInterface $form_state) {
    /** @var $outline \Drupal\outline_entity\OutlineInterface */
    $outline = $form_state->get(['outline', 'outline']);
    $form_state->setRedirectUrl($outline->urlInfo('reset-form'));
  }

}
