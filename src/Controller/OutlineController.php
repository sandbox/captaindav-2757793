<?php

namespace Drupal\outline_entity\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\outline_entity\OutlineInterface;
use Drupal\outline_entity\EntryInterface;

/**
 * Provides route responses for outline_entity.module.
 */
class OutlineController extends ControllerBase {

  /**
   * Returns a form to add a new entry to an outline.
   *
   * @param \Drupal\outline_entity\OutlineInterface $outline
   *   The outline this entry will be added to.
   *
   * @return array
   *   The entry add form.
   */
  public function addForm(OutlineInterface $outline) {
    $entry = $this->entityManager()->getStorage('outline_entry')->create(array('oid' => $outline->id()));
    return $this->entityFormBuilder()->getForm($entry);
  }

  /**
   * Route title callback.
   *
   * @param \Drupal\outline_entity\OutlineInterface $outline
   *   The outline.
   *
   * @return string
   *   The outline label as a render array.
   */
  public function outlineTitle(OutlineInterface $outline) {
    return ['#markup' => $outline->label(), '#allowed_tags' => Xss::getHtmlTagList()];
  }

  /**
   * Route title callback.
   *
   * @param \Drupal\outline_entity\EntryInterface $entry
   *   The outline entry.
   *
   * @return array
   *   The entry label as a render array.
   */
  public function entryTitle(EntryInterface $entry = NULL) {
    return ['#markup' => 'nullname', '#allowed_tags' => Xss::getHtmlTagList()];
    //return ['#markup' => $entry->getName(), '#allowed_tags' => Xss::getHtmlTagList()];
  }

}
