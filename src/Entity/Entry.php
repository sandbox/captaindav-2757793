<?php

namespace Drupal\outline_entity\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\outline_entity\EntryInterface;

/**
 * Defines the outline entry entity.
 *
 * @ContentEntityType(
 *   id = "outline_entry",
 *   label = @Translation("Outline entry"),
 *   bundle_label = @Translation("Outline Entry"),
 *   handlers = {
 *     "storage" = "Drupal\outline_entity\EntryStorage",
 *     "storage_schema" = "Drupal\outline_entity\EntryStorageSchema",
 *     "view_builder" = "Drupal\outline_entity\EntryViewBuilder",
 *     "access" = "Drupal\outline_entity\EntryAccessControlHandler",
 *     "views_data" = "Drupal\outline_entity\EntryViewsData",
 *     "form" = {
 *       "default" = "Drupal\outline_entity\EntryForm",
 *       "delete" = "Drupal\outline_entity\Form\EntryDeleteForm"
 *     },
 *     "translation" = "Drupal\outline_entity\EntryTranslationHandler"
 *   },
 *   base_table = "outline_entry_data",
 *   data_table = "outline_entry_field_data",
 *   uri_callback = "outline_entry_uri",
 *   translatable = TRUE,
 *   entity_keys = {
 *     "id" = "eid",
 *     "bundle" = "oid",
 *     "label" = "name",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid"
 *   },
 *   bundle_entity_type = "outline",
 *   field_ui_base_route = "entity.outline.overview_form",
 *   common_reference_target = TRUE,
 *   links = {
 *     "canonical" = "/outline/entry/{outline_entry}",
 *     "delete-form" = "/outline/entry/{outline_entry}/delete",
 *     "edit-form" = "/outline/entry/{outline_entry}/edit",
 *   },
 *   permission_granularity = "bundle"
 * )
 */
class Entry extends ContentEntityBase implements EntryInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);

    // See if any of the entry's children are about to be become orphans.
    $orphans = array();
    foreach (array_keys($entities) as $eid) {
      if ($children = $storage->loadChildren($eid)) {
        foreach ($children as $child) {
          // If the entry has multiple parents, we don't delete it.
          $parents = $storage->loadParents($child->id());
          if (empty($parents)) {
            $orphans[] = $child->id();
          }
        }
      }
    }

    // Delete entry hierarchy information after looking up orphans but before
    // deleting them so that their children/parent information is consistent.
    $storage->deleteEntryHierarchy(array_keys($entities));

    if (!empty($orphans)) {
      entity_delete_multiple('outline_entry', $orphans);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    // Only change the parents if a value is set, keep the existing values if
    // not.
    if (isset($this->parent->target_id)) {
      $storage->deleteEntryHierarchy(array($this->id()));
      $storage->updateEntryHierarchy($this);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['eid'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Entry ID'))
      ->setDescription(t('The entry ID.'))
      ->setReadOnly(TRUE)
      ->setSetting('unsigned', TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The entry UUID.'))
      ->setReadOnly(TRUE);

    $fields['oid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Outline'))
      ->setDescription(t('The outline to which the entry is assigned.'))
      ->setSetting('target_type', 'outline');

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language'))
      ->setDescription(t('The entry language code.'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'type' => 'hidden',
      ))
      ->setDisplayOptions('form', array(
        'type' => 'language_select',
        'weight' => 2,
      ));

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The entry name.'))
      ->setTranslatable(TRUE)
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -5,
      ))
      ->setDisplayConfigurable('form', TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'))
      ->setDescription(t('A description of the entry.'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'text_default',
        'weight' => 0,
      ))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', array(
        'type' => 'text_textfield',
        'weight' => 0,
      ))
      ->setDisplayConfigurable('form', TRUE);

    $fields['weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Weight'))
      ->setDescription(t('The weight of this entry in relation to other entries.'))
      ->setDefaultValue(0);

    $fields['parent'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Entry Parents'))
      ->setDescription(t('The parents of this entry.'))
      ->setSetting('target_type', 'outline_entry')
      ->setCustomStorage(TRUE);
      //->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)

    $fields['attach'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Attached Entities'))
      ->setDescription(t('Entities attached to this entry.'))
      ->setDisplayOptions('form', array(
          'type' => 'text_textfield',
          'weight' => 0,
      ))
      ->setSetting('target_type', 'node')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED);
      
    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entry was last edited.'))
      ->setTranslatable(TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->get('description')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->set('description', $description);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormat() {
    return $this->get('description')->format;
  }

  /**
   * {@inheritdoc}
   */
  public function setFormat($format) {
    $this->get('description')->format = $format;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->label();
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->get('weight')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->set('weight', $weight);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOutlineId() {
    return $this->get('oid')->target_id;
  }

}
