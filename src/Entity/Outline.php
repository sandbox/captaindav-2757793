<?php

namespace Drupal\outline_entity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\outline_entity\OutlineInterface;

/**
 * Defines the outline entity.
 *
 * @ConfigEntityType(
 *   id = "outline",
 *   label = @Translation("Outline"),
 *   handlers = {
 *     "storage" = "Drupal\outline_entity\OutlineStorage",
 *     "list_builder" = "Drupal\outline_entity\OutlineListBuilder",
 *     "form" = {
 *       "default" = "Drupal\outline_entity\OutlineForm",
 *       "reset" = "Drupal\outline_entity\Form\OutlineResetForm",
 *       "delete" = "Drupal\outline_entity\Form\OutlineDeleteForm"
 *     }
 *   },
 *   admin_permission = "administer outlines",
 *   config_prefix = "outline",
 *   bundle_of = "outline",
 *   entity_keys = {
 *     "id" = "oid",
 *     "label" = "name",
 *     "weight" = "weight"
 *   },
 *   links = {
 *     "add-form" = "/admin/content/outlines/manage/{outline}/add",
 *     "delete-form" = "/admin/content/outlines/manage/{outline}/delete",
 *     "reset-form" = "/admin/content/outlines/manage/{outline}/reset",
 *     "overview-form" = "/admin/content/outlines/manage/{outline}/overview",
 *     "edit-form" = "/admin/content/outlines/manage/{outline}",
 *     "collection" = "/admin/content/outlines",
 *   },
 *   config_export = {
 *     "name",
 *     "oid",
 *     "description",
 *     "hierarchy",
 *     "weight",
 *   }
 * )
 */
class Outline extends ConfigEntityBundleBase implements OutlineInterface {

  /**
   * The outline ID.
   *
   * @var string
   */
  protected $oid;

  /**
   * Name of the outline.
   *
   * @var string
   */
  protected $name;

  /**
   * Description of the outline.
   *
   * @var string
   */
  protected $description;

  /**
   * The type of hierarchy allowed within the outline.
   *
   * Possible values:
   * - OUTLINE_HIERARCHY_DISABLED: No parents.
   * - OUTLINE_HIERARCHY_SINGLE: Single parent.
   * - OUTLINE_HIERARCHY_MULTIPLE: Multiple parents.
   *
   * @var int
   */
  protected $hierarchy = OUTLINE_HIERARCHY_DISABLED;

  /**
   * The weight of this outline in relation to other outlines.
   *
   * @var int
   */
  protected $weight = 0;

  /**
   * {@inheritdoc}
   */
  public function getHierarchy() {
    return $this->hierarchy;
  }

  /**
   * {@inheritdoc}
   */
  public function setHierarchy($hierarchy) {
    $this->hierarchy = $hierarchy;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->oid;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
    parent::preDelete($storage, $entities);

    // Only load Entries without a parent, child entries will get deleted too.
    entity_delete_multiple('outline_entry', $storage->getToplevelEids(array_keys($entities)));
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);

    // Reset caches.
    $storage->resetCache(array_keys($entities));

    if (reset($entities)->isSyncing()) {
      return;
    }

    $outlines = array();
    foreach ($entities as $outline) {
      $outlines[$outline->id()] = $outline->id();
    }
    // Load all Outline module fields and delete those which use only this
    // outline.
    $field_storages = entity_load_multiple_by_properties('field_storage_config', array('module' => 'outline'));
    foreach ($field_storages as $field_storage) {
      $modified_storage = FALSE;
      // Entry reference fields may reference entries from more than one outline.
      foreach ($field_storage->getSetting('allowed_values') as $key => $allowed_value) {
        if (isset($outlines[$allowed_value['outline']])) {
          $allowed_values = $field_storage->getSetting('allowed_values');
          unset($allowed_values[$key]);
          $field_storage->setSetting('allowed_values', $allowed_values);
          $modified_storage = TRUE;
        }
      }
      if ($modified_storage) {
        $allowed_values = $field_storage->getSetting('allowed_values');
        if (empty($allowed_values)) {
          $field_storage->delete();
        }
        else {
          // Update the field definition with the new allowed values.
          $field_storage->save();
        }
      }
    }
  }

}
