<?php

namespace Drupal\outline_entity;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines a Controller class for outline entries.
 */
class EntryStorage extends SqlContentEntityStorage implements EntryStorageInterface {

  /**
   * Array of loaded parents keyed by child entry ID.
   *
   * @var array
   */
  protected $parents = array();

  /**
   * Array of all loaded entry ancestry keyed by ancestor entry ID.
   *
   * @var array
   */
  protected $parentsAll = array();

  /**
   * Array of child entries keyed by parent entry ID.
   *
   * @var array
   */
  protected $children = array();

  /**
   * Array of entry parents keyed by outline ID and child entry ID.
   *
   * @var array
   */
  protected $treeParents = array();

  /**
   * Array of entry ancestors keyed by outline ID and parent entry ID.
   *
   * @var array
   */
  protected $treeChildren = array();

  /**
   * Array of entries in a tree keyed by outline ID and entry ID.
   *
   * @var array
   */
  protected $treeEntrys = array();

  /**
   * Array of loaded trees keyed by a cache id matching tree arguments.
   *
   * @var array
   */
  protected $trees = array();

  /**
   * {@inheritdoc}
   *
   * @param array $values
   *   An array of values to set, keyed by property name. A value for the
   *   outline ID ('oid') is required.
   */
  public function create(array $values = array()) {
    // Save new entries with no parents by default.
    if (empty($values['parent'])) {
      $values['parent'] = array(0);
    }
    $entity = parent::create($values);
    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function resetCache(array $ids = NULL) {
    drupal_static_reset('outline_entry_count_nodes');
    $this->parents = array();
    $this->parentsAll = array();
    $this->children = array();
    $this->treeChildren = array();
    $this->treeParents = array();
    $this->treeEntrys = array();
    $this->trees = array();
    parent::resetCache($ids);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteEntryHierarchy($eids) {
    $this->database->delete('outline_entry_hierarchy')
      ->condition('eid', $eids, 'IN')
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function updateEntryHierarchy(EntityInterface $entry) {
    $query = $this->database->insert('outline_entry_hierarchy')
      ->fields(array('eid', 'parent'));

    foreach ($entry->parent as $parent) {
      $query->values(array(
        'eid' => $entry->id(),
        'parent' => (int) $parent->target_id,
      ));
    }
    $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function loadParents($eid) {
    if (!isset($this->parents[$eid])) {
      $parents = array();
      $query = $this->database->select('outline_entry_field_data', 't');
      $query->join('outline_entry_hierarchy', 'h', 'h.parent = t.eid');
      $query->addField('t', 'eid');
      $query->condition('h.eid', $eid);
      $query->condition('t.default_langcode', 1);
      $query->addTag('entry_access');
      $query->orderBy('t.weight');
      $query->orderBy('t.name');
      if ($ids = $query->execute()->fetchCol()) {
        $parents = $this->loadMultiple($ids);
      }
      $this->parents[$eid] = $parents;
    }
    return $this->parents[$eid];
  }

  /**
   * {@inheritdoc}
   */
  public function loadAllParents($eid) {
    if (!isset($this->parentsAll[$eid])) {
      $parents = array();
      if ($entry = $this->load($eid)) {
        $parents[$entry->id()] = $entry;
        $entries_to_search[] = $entry->id();

        while ($eid = array_shift($entries_to_search)) {
          if ($new_parents = $this->loadParents($eid)) {
            foreach ($new_parents as $new_parent) {
              if (!isset($parents[$new_parent->id()])) {
                $parents[$new_parent->id()] = $new_parent;
                $entries_to_search[] = $new_parent->id();
              }
            }
          }
        }
      }

      $this->parentsAll[$eid] = $parents;
    }
    return $this->parentsAll[$eid];
  }

  /**
   * {@inheritdoc}
   */
  public function loadChildren($eid, $oid = NULL) {
    if (!isset($this->children[$eid])) {
      $children = array();
      $query = $this->database->select('outline_entry_field_data', 't');
      $query->join('outline_entry_hierarchy', 'h', 'h.eid = t.eid');
      $query->addField('t', 'eid');
      $query->condition('h.parent', $eid);
      if ($oid) {
        $query->condition('t.oid', $oid);
      }
      $query->condition('t.default_langcode', 1);
      $query->addTag('entry_access');
      $query->orderBy('t.weight');
      $query->orderBy('t.name');
      if ($ids = $query->execute()->fetchCol()) {
        $children = $this->loadMultiple($ids);
      }
      $this->children[$eid] = $children;
    }
    return $this->children[$eid];
  }

  /**
   * {@inheritdoc}
   */
  public function loadTree($oid, $parent = 0, $max_depth = NULL, $load_entities = FALSE) {
    $cache_key = implode(':', func_get_args());
    if (!isset($this->trees[$cache_key])) {
      // We cache trees, so it's not CPU-intensive to call on an entry and its
      // children, too.
      if (!isset($this->treeChildren[$oid])) {
        $this->treeChildren[$oid] = array();
        $this->treeParents[$oid] = array();
        $this->treeEntrys[$oid] = array();
        $query = $this->database->select('outline_entry_field_data', 't');
        $query->join('outline_entry_hierarchy', 'h', 'h.eid = t.eid');
        $result = $query
          ->addTag('entry_access')
          ->fields('t')
          ->fields('h', array('parent'))
          ->condition('t.oid', $oid)
          ->condition('t.default_langcode', 1)
          ->orderBy('t.weight')
          ->orderBy('t.name')
          ->execute();
        foreach ($result as $entry) {
          $this->treeChildren[$oid][$entry->parent][] = $entry->eid;
          $this->treeParents[$oid][$entry->eid][] = $entry->parent;
          $this->treeEntrys[$oid][$entry->eid] = $entry;
        }
      }

      // Load full entities, if necessary. The entity controller statically
      // caches the results.
      $entry_entities = array();
      if ($load_entities) {
        $entry_entities = $this->loadMultiple(array_keys($this->treeEntrys[$oid]));
      }

      $max_depth = (!isset($max_depth)) ? count($this->treeChildren[$oid]) : $max_depth;
      $tree = array();

      // Keeps track of the parents we have to process, the last entry is used
      // for the next processing step.
      $process_parents = array();
      $process_parents[] = $parent;

      // Loops over the parent entries and adds its children to the tree array.
      // Uses a loop instead of a recursion, because it's more efficient.
      while (count($process_parents)) {
        $parent = array_pop($process_parents);
        // The number of parents deentryines the current depth.
        $depth = count($process_parents);
        if ($max_depth > $depth && !empty($this->treeChildren[$oid][$parent])) {
          $has_children = FALSE;
          $child = current($this->treeChildren[$oid][$parent]);
          do {
            if (empty($child)) {
              break;
            }
            $entry = $load_entities ? $entry_entities[$child] : $this->treeEntrys[$oid][$child];
            if (isset($this->treeParents[$oid][$load_entities ? $entry->id() : $entry->eid])) {
              // Clone the entry so that the depth attribute remains correct
              // in the event of multiple parents.
              $entry = clone $entry;
            }
            $entry->depth = $depth;
            unset($entry->parent);
            $eid = $load_entities ? $entry->id() : $entry->eid;
            $entry->parents = $this->treeParents[$oid][$eid];
            $tree[] = $entry;
            if (!empty($this->treeChildren[$oid][$eid])) {
              $has_children = TRUE;

              // We have to continue with this parent later.
              $process_parents[] = $parent;
              // Use the current entry as parent for the next iteration.
              $process_parents[] = $eid;

              // Reset pointers for child lists because we step in there more
              // often with multi parents.
              reset($this->treeChildren[$oid][$eid]);
              // Move pointer so that we get the correct entry the next time.
              next($this->treeChildren[$oid][$parent]);
              break;
            }
          } while ($child = next($this->treeChildren[$oid][$parent]));

          if (!$has_children) {
            // We processed all entries in this hierarchy-level, reset pointer
            // so that this function works the next time it gets called.
            reset($this->treeChildren[$oid][$parent]);
          }
        }
      }
      $this->trees[$cache_key] = $tree;
    }
    return $this->trees[$cache_key];
  }

  /**
   * {@inheritdoc}
   */
  public function nodeCount($oid) {
    $query = $this->database->select('outline_index', 'ti');
    $query->addExpression('COUNT(DISTINCT ti.nid)');
    $query->leftJoin('outline_entry_data', 'td', 'ti.eid = td.eid');
    $query->condition('td.oid', $oid);
    $query->addTag('outline_node_count');
    return $query->execute()->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function resetWeights($oid) {
    $this->database->update('outline_entry_field_data')
      ->fields(array('weight' => 0))
      ->condition('oid', $oid)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getNodeEntrys(array $nids, array $outlines = array(), $langcode = NULL) {
    $query = db_select('outline_entry_field_data', 'td');
    $query->innerJoin('outline_index', 'tn', 'td.eid = tn.eid');
    $query->fields('td', array('eid'));
    $query->addField('tn', 'nid', 'node_nid');
    $query->orderby('td.weight');
    $query->orderby('td.name');
    $query->condition('tn.nid', $nids, 'IN');
    $query->addTag('entry_access');
    if (!empty($outlines)) {
      $query->condition('td.oid', $outlines, 'IN');
    }
    if (!empty($langcode)) {
      $query->condition('td.langcode', $langcode);
    }

    $results = array();
    $all_eids = array();
    foreach ($query->execute() as $entry_record) {
      $results[$entry_record->node_nid][] = $entry_record->eid;
      $all_eids[] = $entry_record->eid;
    }

    $all_entries = $this->loadMultiple($all_eids);
    $entries = array();
    foreach ($results as $nid => $eids) {
      foreach ($eids as $eid) {
        $entries[$nid][$eid] = $all_entries[$eid];
      }
    }
    return $entries;
  }

  /**
   * {@inheritdoc}
   */
  public function __sleep() {
    $vars = parent::__sleep();
    // Do not serialize static cache.
    unset($vars['parents'], $vars['parentsAll'], $vars['children'], $vars['treeChildren'], $vars['treeParents'], $vars['treeEntrys'], $vars['trees']);
    return $vars;
  }

  /**
   * {@inheritdoc}
   */
  public function __wakeup() {
    parent::__wakeup();
    // Initialize static caches.
    $this->parents = array();
    $this->parentsAll = array();
    $this->children = array();
    $this->treeChildren = array();
    $this->treeParents = array();
    $this->treeEntrys = array();
    $this->trees = array();
  }

}
