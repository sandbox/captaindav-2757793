<?php

namespace Drupal\outline_entity;

use Drupal\Core\Config\Entity\ConfigEntityStorage;

/**
 * Defines a storage handler class for outlines.
 */
class OutlineStorage extends ConfigEntityStorage implements OutlineStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function resetCache(array $ids = NULL) {
    drupal_static_reset('outline_get_names');
    parent::resetCache($ids);
  }

  /**
   * {@inheritdoc}
   */
  public function getToplevelEids($oids) {
    return db_query('SELECT t.eid FROM {entry_data} ed INNER JOIN {outline_entry_hierarchy} eh ON eh.eid = ed.oid WHERE t.oid IN ( :oids[] ) AND eh.parent = 0', array(':oids[]' => $oids))->fetchCol();
  }

}
