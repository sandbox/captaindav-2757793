<?php

namespace Drupal\outline_entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an outline entity.
 */
interface OutlineInterface extends ConfigEntityInterface {

  /**
   * Returns the outline description.
   *
   * @return string
   *   The outline description.
   */
  public function getDescription();
}
