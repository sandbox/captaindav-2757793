<?php

namespace Drupal\outline_entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining an outline entry entity.
 */
interface EntryInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Gets the entry's description.
   *
   * @return string
   *   The entry description.
   */
  public function getDescription();

  /**
   * Sets the entry's description.
   *
   * @param string $description
   *   The entry's description.
   *
   * @return $this
   */
  public function setDescription($description);

  /**
   * Gets the text format name for the entry's description.
   *
   * @return string
   *   The text format name.
   */
  public function getFormat();

  /**
   * Sets the text format name for the entry's description.
   *
   * @param string $format
   *   The entry's description text format.
   *
   * @return $this
   */
  public function setFormat($format);

  /**
   * Gets the name of the entry.
   *
   * @return string
   *   The name of the entry.
   */
  public function getName();

  /**
   * Sets the name of the entry.
   *
   * @param int $name
   *   The entry's name.
   *
   * @return $this
   */
  public function setName($name);

  /**
   * Gets the weight of this entry.
   *
   * @return int
   *   The weight of the entry.
   */
  public function getWeight();

  /**
   * Gets the weight of this entry.
   *
   * @param int $weight
   *   The entry's weight.
   *
   * @return $this
   */
  public function setWeight($weight);

  /**
   * Get the outline id this entry belongs to.
   *
   * @return int
   *   The id of the outline.
   */
  public function getOutlineId();

}
