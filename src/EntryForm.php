<?php

namespace Drupal\outline_entity;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Base for handler for outline entry edit forms.
 */
class EntryForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $entry = $this->entity;
    $outl_storage = $this->entityManager->getStorage('outline');
    $outline_storage = $this->entityManager->getStorage('outline_entry');
    $outline = $outl_storage->load($entry->bundle());

    $parent = array_keys($outline_storage->loadParents($entry->id()));
    $form_state->set(['outline', 'parent'], $parent);
    $form_state->set(['outline', 'outline'], $outline);

    $form['relations'] = array(
      '#type' => 'details',
      '#title' => $this->t('Relations'),
      '#open' => $outline->getHierarchy() == OUTLINE_HIERARCHY_MULTIPLE,
      '#weight' => 10,
    );

    // \Drupal\outline_entity\EntryStorageInterface::loadTree() and
    // \Drupal\outline_entity\EntryStorageInterface::loadParents() may contain large
    // numbers of items so we check for outline_entity.settings:override_selector
    // before loading the full outline. Contrib modules can then intercept
    // before hook_form_alter to provide scalable alternatives.
    if (!$this->config('outline_entity.settings')->get('override_selector')) {
      $parent = array_keys($outline_storage->loadParents($entry->id()));
      $children = $outline_storage->loadTree($outline->id(), $entry->id());

      // An entry can't be the child of itself, nor of its children.
      foreach ($children as $child) {
        $exclude[] = $child->eid;
      }
      $exclude[] = $entry->id();

      $tree = $outline_storage->loadTree($outline->id());
      $options = array('<' . $this->t('root') . '>');
      if (empty($parent)) {
        $parent = array(0);
      }

      foreach ($tree as $item) {
        if (!in_array($item->eid, $exclude)) {
          $options[$item->eid] = str_repeat('-', $item->depth) . $item->name;
        }
      }

      $form['relations']['parent'] = array(
        '#type' => 'select',
        '#title' => $this->t('Parent entries'),
        '#options' => $options,
        '#default_value' => $parent,
        '#multiple' => TRUE,
      );
    }

    $form['relations']['weight'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Weight'),
      '#size' => 6,
      '#default_value' => $entry->getWeight(),
      '#description' => $this->t('Entries are displayed in ascending order by weight.'),
      '#required' => TRUE,
    );

    $form['oid'] = array(
      '#type' => 'value',
      '#value' => $outline->id(),
    );

    $form['eid'] = array(
      '#type' => 'value',
      '#value' => $entry->id(),
    );

    return parent::form($form, $form_state, $entry);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Ensure numeric values.
    if ($form_state->hasValue('weight') && !is_numeric($form_state->getValue('weight'))) {
      $form_state->setErrorByName('weight', $this->t('Weight value must be numeric.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntity(array $form, FormStateInterface $form_state) {
    $entry = parent::buildEntity($form, $form_state);

    // Prevent leading and trailing spaces in entry names.
    $entry->setName(trim($entry->getName()));

    // Assign parents with proper delta values starting from 0.
    $entry->parent = array_keys($form_state->getValue('parent'));

    return $entry;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entry = $this->entity;

    $result = $entry->save();

    $link = $entry->link($this->t('Edit'), 'edit-form');
    switch ($result) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created new entry %entry.', array('%entry' => $entry->getName())));
        $this->logger('outline')->notice('Created new entry %entry.', array('%entry' => $entry->getName(), 'link' => $link));
        break;
      case SAVED_UPDATED:
        drupal_set_message($this->t('Updated entry %entry.', array('%entry' => $entry->getName())));
        $this->logger('outline')->notice('Updated entry %entry.', array('%entry' => $entry->getName(), 'link' => $link));
        break;
    }

    $current_parent_count = count($form_state->getValue('parent'));
    $previous_parent_count = count($form_state->get(['outline', 'parent']));
    // Root doesn't count if it's the only parent.
    if ($current_parent_count == 1 && $form_state->hasValue(array('parent', 0))) {
      $current_parent_count = 0;
      $form_state->setValue('parent', array());
    }

    // If the number of parents has been reduced to one or none, do a check on the
    // parents of every entry in the outline value.
    $outline = $form_state->get(['outline', 'outline']);
    if ($current_parent_count < $previous_parent_count && $current_parent_count < 2) {
      outline_check_outline_hierarchy($outline, $form_state->getValues());
    }
    // If we've increased the number of parents and this is a single or flat
    // hierarchy, update the outline immediately.
    elseif ($current_parent_count > $previous_parent_count && $outline->getHierarchy() != OUTLINE_HIERARCHY_MULTIPLE) {
      $outline->setHierarchy($current_parent_count == 1 ? OUTLINE_HIERARCHY_SINGLE : OUTLINE_HIERARCHY_MULTIPLE);
      $outline->save();
    }

    $form_state->setValue('eid', $entry->id());
    $form_state->set('eid', $entry->id());
  }

}
