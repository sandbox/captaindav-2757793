/**
 * @file
 * Outline behaviors.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Move a block in the blocks table from one region to another.
   *
   * This behavior is dependent on the tableDrag behavior, since it uses the
   * objects initialized in that behavior to update the row.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the drag behavior to a applicable table element.
   */
  Drupal.behaviors.entryDrag = {
    attach: function (context, settings) {
      var backStep = settings.outline.backStep;
      var forwardStep = settings.outline.forwardStep;
      // Get the blocks tableDrag object.
      var tableDrag = Drupal.tableDrag.outline;
      var $table = $('#outline');
      var rows = $table.find('tr').length;

      // When a row is swapped, keep previous and next page classes set.
      tableDrag.row.prototype.onSwap = function (swappedRow) {
        $table.find('tr.outline-entry-preview').removeClass('outline-entry-preview');
        $table.find('tr.outline-entry-divider-top').removeClass('outline-entry-divider-top');
        $table.find('tr.outline-entry-divider-bottom').removeClass('outline-entry-divider-bottom');

        var tableBody = $table[0].tBodies[0];
        if (backStep) {
          for (var n = 0; n < backStep; n++) {
            $(tableBody.rows[n]).addClass('outline-entry-preview');
          }
          $(tableBody.rows[backStep - 1]).addClass('outline-entry-divider-top');
          $(tableBody.rows[backStep]).addClass('outline-entry-divider-bottom');
        }

        if (forwardStep) {
          for (var k = rows - forwardStep - 1; k < rows - 1; k++) {
            $(tableBody.rows[k]).addClass('outline-entry-preview');
          }
          $(tableBody.rows[rows - forwardStep - 2]).addClass('outline-entry-divider-top');
          $(tableBody.rows[rows - forwardStep - 1]).addClass('outline-entry-divider-bottom');
        }
      };
    }
  };

})(jQuery, Drupal);
